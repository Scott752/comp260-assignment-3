﻿using UnityEngine;
using System.Collections;

public class SheepSpawn : MonoBehaviour {
	public Transform target;


	public SheepMove SheepPrefab;
	//public PlayerMove player;
	public int nSheep = 50; 

	public float xMin, yMin;
	public float width, height;
	private float timeTillNext,TimeSinceLast = 0f;
	public float minSheepP, MaxSheepP;
	private int SheepNum = 0;
	  
	public int Score = 0;
	// Use this for initialization
	void Start () {
		createSheep ();



	}
	public void DestroySheep(Vector2 center, float radius){
		for (int i = 0; i < transform.childCount; i++) {
			Transform child = transform.GetChild (i);
			Vector2 v = (Vector2)child.position - center;
			if (v.magnitude <= radius) {
				Destroy (child.gameObject);
			}

		}

	}
	void createSheep(){
		SheepMove Sheep = Instantiate (SheepPrefab);

		Sheep.transform.parent = transform;
		Sheep.gameObject.name = "Sheep " + SheepNum;

		float x = xMin + Random.value * width;
		float y = yMin + Random.value * height;
		Sheep.transform.position = new Vector2 (x, y);

		timeTillNext =  Mathf.Lerp (minSheepP, MaxSheepP, Random.value);
		SheepNum++;
		TimeSinceLast = 0f;
	}
	// Update is called once per frame
	void Update () {
		TimeSinceLast += Time.deltaTime;
		if (TimeSinceLast >= timeTillNext) {
			createSheep ();
		}
		//if the sheep are in the fenced area then destroy the sheep and add a point
		/*if (Sheep.transform.position <= 2 && Sheep) {
			DestroySheep ();
			Score ++;
		}*/
	}
}
